[//]:# (This Readme is intended for viewing on the Bitbucket flavor of markdown)
[//]:# (Some things may not render properly on other markdown flavors.)
[//]:# (Known differences:)
[//]:#    (Angle brackets do not display with Github Markdown)

[//]:# (This Project is hosted publicly at https://bitbucket.org/xtodpep_stew/minifs)

# A Posix implementation of <filesystem> in C++11

This is an implementation of C++17's <filesystem>. It's a work in progress.  
I've tried my best to match exactly what is presented in: http://en.cppreference.com/w/cpp/experimental/fs  
It is based on the posix header <dirent.h>  

Listed here are the implemented library components  
Certain elements are not part of the standard and are temporary,  
these components are ***bold italicized***

---
## How to Use:
+ Run Make on a Linux system with GCC and C++11 support
    + Other compilers can be specified with the compiler variable in the makefile
+ Then include "filesystem/filesystem.hpp" in your project

---
## Classes:
+ **directory_entry**
    - Constructors:
        * directory_entry() noexcept = default;
        * ***directory_entry(const std::string& path);***
        * directory_entry(const directory_entry&) = default;
        * directory_entry(directory_entry&&) noexcept = default;
    - Destructor:
        * (default destructor)
    - Member functions:
        * Modifiers:
            + operator=
        * Observers:
            + exists
            + is_block_file
            + is_character_file
            + is_directory
            + is_fifo
            + is_other
            + is_regular_file
            + is_socket
            + is_symlink
            + file_size
            + ***get_status***
        * Comparison:
            + operator==
            + operator!=
+ **directory_iterator**
    - Constructors:
        * directory_iterator() noexcept;
        * ***directory_iterator(const std::string& path);***
        * directory_iterator(const directory_iterator&) = default;
        * directory_iterator(directory_iterator&&) = default;
    * Destructor:
        * (default destructor)
    * Member functions:
        * operator=
        * operator*
        * operator++
    * Non-member functions
        * begin(minifs::directory_iterator)
        * end(minifs::directory_iterator)
+ **filesystem_error**
    - Constructors:
        * ***filesystem_error(const std::string& what_arg, std::error_code ec);***
    - Member functions:
        * what
