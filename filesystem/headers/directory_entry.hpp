#pragma once
#include <string>
#include <cstdint>
#include "posix_includes.hpp"

namespace minifs
{
    class directory_entry
    {
    private:
        std::string name;
        bool valid;
        struct stat file_status;
    public:
        directory_entry(const std::string& path);
        directory_entry() noexcept = default;
        directory_entry(const directory_entry&) = default;
        directory_entry(directory_entry&&) noexcept = default;
        directory_entry& operator=(const directory_entry&) = default;
        directory_entry& operator=(directory_entry&&) noexcept = default;
        bool exists() const;
        bool is_block_file() const;
        bool is_directory() const;
        bool is_character_file() const;
        bool is_regular_file() const;
        bool is_socket() const;
        bool is_symlink() const;
        bool is_fifo() const;
        const struct stat& get_status() const;
        const std::string& path() const noexcept;
        bool operator==(const directory_entry&) const noexcept;
        bool operator!=(const directory_entry&) const noexcept;
        std::uintmax_t file_size() const;
    };
    std::ostream& operator<<(std::ostream&, const directory_entry&);
}
