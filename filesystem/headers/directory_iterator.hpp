#pragma once
#include <string>
#include <memory>
#include "directory_stream.hpp"
#include "directory_entry.hpp"
#include "filesystem_error.hpp"

namespace minifs
{
    class directory_iterator
    {
    private:
        std::string root_path;
        std::shared_ptr<directory_stream> dir_stream;
        directory_entry current_entry; //the current entry
    public:
        directory_iterator(const std::string& path);
        directory_iterator() noexcept = default;
        directory_iterator(const directory_iterator&) = default;
        directory_iterator(directory_iterator&&) = default;
        bool operator==(const directory_iterator& rhs) const;
        bool operator!=(const directory_iterator& rhs) const;
        directory_iterator& operator++();
        const directory_entry& operator*() const;
        const directory_entry* operator->() const;
        using value_type = directory_entry;
        using pointer = const directory_entry*;
        using reference = const directory_entry&;
        using iterator_category = std::input_iterator_tag;
    };

    directory_iterator begin(directory_iterator) noexcept;
    directory_iterator end(const directory_iterator&) noexcept;
}
