#pragma once
#include <string>
#include "posix_includes.hpp"
#include "directory_entry.hpp"

namespace minifs
{
    //helper class to manage DIR*
    class directory_stream
    {
    private:
        DIR* directory_ptr;
        std::string root_path;
        bool valid_dir;
    public:
        directory_stream(const std::string& path);
        ~directory_stream();
        directory_entry advance(); //return next entry in stream
    };
}
