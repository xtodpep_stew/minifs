#pragma once
#include <exception>
#include <system_error>
#include "path.hpp"

namespace minifs
{
    class filesystem_error : public std::system_error
    {
        const std::string message;
        //const minifs::path p1, p2;
        const std::error_code ec;
    public:
        filesystem_error(const std::string& what_arg, std::error_code ec);
        // filesystem_error(const std::string& what_arg, const minifs::path& p1,
        //                  std::error_code ec);
        // filesystem_error(const std::string& what_arg,
        //                  const minifs::path& p1,
        //                  const minifs::path& p2, std::error_code ec);
        // const path& path1() const noexcept;
        // const path& path2() const noexcept;
        const char* what() const noexcept override;
    };
}
