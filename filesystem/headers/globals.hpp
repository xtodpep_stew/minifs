#pragma once
namespace minifs
{
    //this strange declaration of globals
    //is to prevent ephemeral linker errors
    template<class = void>
    struct globals
    {
        static std::string current_dir;
        static std::string parent_dir;
        static std::string delimiter;
    };

    template<> std::string globals<>::current_dir = ".";
    template<> std::string globals<>::parent_dir = "..";
    template<> std::string globals<>::delimiter = "/";

    static auto& current_dir = globals<>::current_dir;
    static auto& parent_dir = globals<>::parent_dir;
    static auto& delimiter = globals<>::delimiter;
}
