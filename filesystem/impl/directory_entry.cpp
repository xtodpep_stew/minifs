#include "directory_entry.hpp"

minifs::directory_entry::directory_entry(const std::string& path)
{
    name = path;
    if(stat(name.c_str(), &file_status) == 0)
        valid = true;
    else
        valid = false;
}

bool minifs::directory_entry::exists() const { return valid; }

bool minifs::directory_entry::is_regular_file() const
{
    return S_ISREG(file_status.st_mode);
}

bool minifs::directory_entry::is_block_file() const
{
    return S_ISBLK(file_status.st_mode);
}

bool minifs::directory_entry::is_directory() const
{
    return S_ISDIR(file_status.st_mode);
}

bool minifs::directory_entry::is_character_file() const
{
    return S_ISCHR(file_status.st_mode);
}

bool minifs::directory_entry::is_socket() const
{
    return S_ISSOCK(file_status.st_mode);
}

bool minifs::directory_entry::is_symlink() const
{
    return S_ISLNK(file_status.st_mode);
}

bool minifs::directory_entry::is_fifo() const
{
    return S_ISFIFO(file_status.st_mode);
}

const struct stat& minifs::directory_entry::get_status() const
{
    return file_status;
}

std::uintmax_t minifs::directory_entry::file_size() const
{
    return static_cast<std::uintmax_t>(file_status.st_size);
}

const std::string& minifs::directory_entry::path() const noexcept
{
    return name;
}

bool minifs::directory_entry::operator==(const directory_entry& rhs)
const noexcept
{
    return this->name == rhs.name;
}

bool minifs::directory_entry::operator!=(const directory_entry& rhs)
const noexcept
{
    return !(*this == rhs);
}

std::ostream& minifs::operator<<(std::ostream& os, const directory_entry& de)
{
    return os << de.path();
}
