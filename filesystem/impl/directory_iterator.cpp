#include "directory_iterator.hpp"

minifs::directory_iterator::directory_iterator(const std::string& path) :
root_path{path}
{
    directory_entry path_check{root_path};
    if(!path_check.exists())
    {
        //std::cout << "ctor: fde\n";
        throw minifs::filesystem_error{
            "file does not exist",
            std::error_code{errno, std::system_category()}
        };
    }
    dir_stream = std::make_shared<directory_stream>(root_path);
    current_entry = dir_stream->advance();
}

bool minifs::directory_iterator::operator==(const directory_iterator& rhs)
const
{
    return this->current_entry == rhs.current_entry;
}

bool minifs::directory_iterator::operator!=(const directory_iterator& rhs)
const
{
    return !(*this == rhs);
}

minifs::directory_iterator& minifs::directory_iterator::operator++()
{
    current_entry = std::move(dir_stream->advance());
    return *this;
}

const minifs::directory_entry& minifs::directory_iterator::operator*() const
{
    return this->current_entry;
}

const minifs::directory_entry* minifs::directory_iterator::operator->() const
{
    return &(this->current_entry);
}

minifs::directory_iterator minifs::begin(minifs::directory_iterator iter)
noexcept
{
    return iter;
}

minifs::directory_iterator minifs::end(const minifs::directory_iterator&)
noexcept
{
    return minifs::directory_iterator{};
}
