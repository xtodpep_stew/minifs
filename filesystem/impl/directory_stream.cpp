#include "directory_stream.hpp"
#include "directory_entry.hpp"
#include "globals.hpp"

minifs::directory_stream::directory_stream(const std::string& path)
{
    root_path = path;
    directory_ptr = opendir(path.c_str());
    if(directory_ptr)
        valid_dir = true;
    else
        valid_dir = false;
}

minifs::directory_stream::~directory_stream()
{
    while((closedir(directory_ptr) != 0) && errno == EINTR) {}
}

minifs::directory_entry minifs::directory_stream::advance()
{

    dirent* next_entry = readdir(directory_ptr);
    if (next_entry)
    {
        //unless set otherwise tbi...
        if(next_entry->d_name == minifs::current_dir
        || next_entry->d_name == minifs::parent_dir)
        {
            return advance();
        }
        else
        {
            return minifs::directory_entry(root_path + minifs::delimiter +
                                    next_entry->d_name);
        }
     }
    return minifs::directory_entry();
}
