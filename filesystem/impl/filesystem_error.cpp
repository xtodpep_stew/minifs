#include "filesystem_error.hpp"

minifs::filesystem_error::filesystem_error(const std::string& what_arg,
                                           std::error_code ec) :
    message{what_arg}, ec{ec} {}

// minifs::filesystem_error::filesystem_error(const std::string& what_arg,
//                                            const minifs::path& p1,
//                                            std::error_code ec) :
//     message{what_arg.c_str()}, p1{p1}, ec{ec} {}
//
// minifs::filesystem_error::filesystem_error(const std::string& what_arg,
//                                            const minifs::path& p1,
//                                            const minifs::path& p2,
//                                            std::error_code ec) :
//     message{what_arg.c_str()}, p1{p1}, p2{p2}, ec{ec} {}

// const minifs::path& minifs::filesystem_error::path1() const noexcept
// {
//     return p1;
// }
//
// const minifs::path& minifs::filesystem_error::path2() const noexcept
// {
//     return p2;
// }

const char* minifs::filesystem_error::what() const noexcept
{
    return message.c_str();
}
