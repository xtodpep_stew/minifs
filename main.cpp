#include <algorithm>
#include <iostream>
#include "filesystem/filesystem.hpp"

int main(int argc, char* argv[])
{
    //Sample of usage
    namespace fs = minifs;
    if(argc != 2)
    {
        std::cerr << "Usage: "   << argv[0] << " filename\n";
        std::exit(EXIT_FAILURE);
    }

    //Print some useful information about the directory argv[1]
    try
    {
        fs::directory_iterator dir{std::string{argv[1]}};
        std::cout << std::boolalpha;
        for(auto& p : dir)
            std::cout << p << ":\n"
                << "\tsize: \t\t"       << p.file_size() << "\n"
                << "\tis_dir: \t"       << p.is_directory() << "\n"
                << "\tis_regular: \t"   << p.is_regular_file() << "\n"
                << "\tis_symlink: \t"   << p.is_symlink() << "\n\n";
    }
    catch(minifs::filesystem_error e)
    {
        std::cout << e.what() << "\n";
    }
}
