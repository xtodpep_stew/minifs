w_flags = -Wall -Werror
o_flags = -O3
compiler = g++
cppstandard = -std=c++11
driver_name = sample_usage

lib_assembler = ar rvs
lib_name = minifs.a
lib_objects = directory_iterator.o directory_entry.o directory_stream.o filesystem_error.o
lib_dependencies = filesystem/*/*

$(driver_name): main.o minifs.a filesystem/*/*
	$(compiler) \
		-o $(driver_name) \
		main.o $(lib_name) \
		$(cppstandard) \
		$(o_flags) \
		$(w_flags)

main.o: main.cpp
	$(compiler) \
		-c main.cpp \
		$(cppstandard) \
		$(o_flags) \
		$(w_flags)

$(lib_name): $(lib_objects)
	$(lib_assembler) $(lib_name) $(lib_objects)

directory_iterator.o: $(lib_dependencies)
	$(compiler) \
		-c filesystem/impl/directory_iterator.cpp \
		-I./filesystem/headers \
		$(cppstandard) \
		$(o_flags) \
		$(w_flags)

directory_entry.o: $(lib_dependencies)
	$(compiler) \
		-c filesystem/impl/directory_entry.cpp \
		-I./filesystem/headers \
		$(cppstandard) \
		$(o_flags) \
		$(w_flags)

directory_stream.o: $(lib_dependencies)
	$(compiler) \
		-c filesystem/impl/directory_stream.cpp \
		-I./filesystem/headers \
		$(cppstandard) \
		$(o_flags) \
		$(w_flags)

filesystem_error.o: $(lib_dependencies)
	$(compiler) \
		-c filesystem/impl/filesystem_error.cpp \
		-I./filesystem/headers \
		$(cppstandard) \
		$(o_flags) \
		$(w_flags)

clean:
	rm *.o
	rm $(lib_name)
